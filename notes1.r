# Functioning R Notes from CSUCHICO's MATH314 Class
#        - Upkeep by gerrittdorland (github)
#        - I will pretty this up in my free time.

#GETTING R ON LINUX COMMAND-LINE (Ubuntu):
#
#        1. Ubuntu's version of R is way out of date. Do this to get 
#           a current version:
#               (1): sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9 
#               (2): sudo add-apt-repository ppa:marutter/rdev
#               (3): sudo apt-get update 
#               (4): sudo apt-get upgrade  
#               (5): sudo apt-get install r-base r-base-dev  
#
#        2. Get ggplot2 library
#               (1): Open Terminal
#               (2): Type "sudo R"
#               (3): Once R loads, type "install.packages("ggplot2")"
#               (4): Select a mirror from the pop-up window
#               (5): Wait until it completes. Ready-to-go!
#        3. If this file does not run after running these steps, only
#           faith can help you now.

#load our common library (qplot)
library(ggplot2)

#Get the file. HTTPS does not work with R, so we have to cURL it.
cat("Ignore the cURL output...\n\n")
download.file("https://roualdes.us/data/email.csv", destfile = "/tmp/test.csv", method = "curl")

cat("\n\n------------------------------\nEXAMPLES OF R\n------------------------------\n\n")

#parse the .csv
email <- read.csv("/tmp/test.csv")

val <- is.numeric(email$num_char)

#cat concactenates stuff. it functions more like your classic printf
#looks to be only for output
#The 'print' function will not do mixed output or escape characters
cat("Hello, World! Is email$num_char numeric?: ", val, "\n\n")

#---------------------Class 2--------------------------------------

cat("\t~~~\tClass 2 Notes\t~~~\t\n\n")


print("Let's check that the mean function has the same output as regular math...")
phat <- sum(email$spam)/length(email$spam)
phat == mean(email$spam, na.rm = TRUE)
cat("\n")

#mean, median and mode are all available as functions. See mean above.
#It's all called the same way.

#we can find percentiles by using the function quantile
#        the c function is concactenating numbers - making a vector.
cat("\n")
print("Let's use Quantile to get a percentage:")
cat("\n")

quantile(email$num_char, probs=c(0.25, 0.5, 0.75))

#counts observations
table(email$spam)

#gives you the proportions
prop.table(table(email$spam))

#get a two-way table (Contingency table)
round(prop.table(table(email$spam, email$number)), 2)

#To print out a plot, you -must- wrap the plot in a 'print()' function.
#       Really lame, but whatever.
#IMPORTANT NOTE:
#       PRINTING PLOTS WILL NOT WORK WHEN YOU RUN
#               THE SOURCE VIA 'Rsource'.
#       YOU -MUST- START R BY TYPING 'R' + <enter>
#               THEN ENTERING 'sorce("<myfile.r>")' + <enter>
#       Remember that 'q()' will quit.

print(qplot(num_char, data=email, geom="histogram"))
print("Press ENTER to get next graph.")
pause <- readline()
#Consecutive calls to qplot will overwrite eachother.
#       Gotta wait for ENTER or something, or we'll only see
#       the last graph.

print(qplot(num_char, data=email, geom="bar"))
print("Press ENTER to get next graph.")
pause <- readline()

print(qplot(num_char, data=email, geom="histogram", binwidth=10))

#If you want to save...
#ggsave("freedom.png", qplot(num_char, data=email, geom="histogram", binwidth=10), device=NULL)

#---------------------------DAY 3-----------------------------------

# data frames are like arrays, split like so:
#   data[rows,columns].
# A blank entry grabs every single one. So:
#   data[,c("uptake", "conc")] will get every row, but a limited # columns
print("Get the row labels:")
cat("\n")

rownames(email)


print("Get the column labels:")
cat("\n")
colnames(email)
cat("\n\n")

cat("===========================================\n\n")
print("Print up to 6 results from column 'winner'")
cat("\n")
head(email[,c("winner")])
cat("\n===========================================\n\n")

print("Print up to 6 results from row '7'")
head(email[c("7"),])
cat("\n")
cat("\n===========================================\n\n")

print("Print up to 6 results from row '7', column 'winner'")
head(email[c("7"),c("winner")])
cat("\n")
cat("\n===========================================\n\n")

#head(email[,

cat("\n\n------------------------------\nEND EXAMPLES OF R\n------------------------------\n\n")
